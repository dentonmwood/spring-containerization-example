import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Person } from '../model/person';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  constructor(
    private http: HttpClient
  ) { }

  private restUrl = 'http://localhost:8080/persons';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  getPersons(): Observable<Person[]> {
    return this.http.get<Person[]>(this.restUrl)
      .pipe(
        catchError(this.handleError<Person[]>('getPersons', []))
      );
  }

  getPerson(id: number): Observable<Person> {
    const url = `${this.restUrl}/${id}`;
    return this.http.get<Person>(url).pipe(
      catchError(this.handleError<Person>(`getPerson id=${id}`))
    );
  }

  updatePerson(person: Person): Observable<any> {
    return this.http.post(this.restUrl, JSON.stringify(person), this.httpOptions).pipe(
      catchError(this.handleError<any>('updatePerson'))
    );
  }

  addPerson(person: Person): Observable<any> {
    return this.http.put(this.restUrl, JSON.stringify(person), this.httpOptions).pipe(
      catchError(this.handleError<any>('addPerson'))
    );
  }

  deletePerson(id: number): Observable<any> {
    return this.http.delete(`${this.restUrl}/${id}`).pipe(
      catchError(this.handleError<any>('deletePerson'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(error);
      return of(result as T);
    };
  }
}