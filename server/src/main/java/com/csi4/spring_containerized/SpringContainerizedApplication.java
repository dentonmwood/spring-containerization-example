package com.csi4.spring_containerized;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringContainerizedApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringContainerizedApplication.class, args);
	}

}
