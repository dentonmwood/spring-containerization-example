import { Component, OnInit } from '@angular/core';
import { Person } from '../../model/person';
import { PersonService } from '../../service/person.service';

@Component({
  selector: 'app-add-person',
  templateUrl: './add-person.component.html',
  styleUrls: ['./add-person.component.css']
})
export class AddPersonComponent implements OnInit {

  private person: Person;

  constructor(private personService: PersonService) { }

  ngOnInit() {
    this.person = new Person();
  }

  save(): void {
    this.personService.addPerson(this.person).subscribe(() => this.ngOnInit());
  }

}
