package com.csi4.spring_containerized;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(path = "/persons", produces = "application/json")
public class PersonController {
    private PersonRepository personRepository;

    public PersonController(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @GetMapping(produces = "application/json")
    public Iterable<Person> get() {
        return this.personRepository.findAll();
    }

    @GetMapping(path = "/{id}", produces = "application/json")
    public Person getById(@PathVariable Long id) {
        return this.personRepository.findById(id).orElse(null);
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public Person post(@RequestBody Person person) {
        return this.personRepository.save(person);
    }
}
