package com.csi4.spring_containerized;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Person {
    @Id
    private final Long id;
    private final String name;
    private final Integer age;
    private final String address;
    private final String phoneNumber;
}
