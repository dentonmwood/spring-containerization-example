export class Person {
    id: number;
    name: string;
    age: number;
    address: string;
    phoneNumber: string;
}
